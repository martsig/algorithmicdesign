package dijkstra;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import model.ConnectionModel;
import model.StationModel;

public class DijkstraAlgorithm {
	
	private ArrayList<StationModel> settled;
	private ArrayList<StationModel> unsettled;
	private HashMap<StationModel, Integer> distance;
	
	public DijkstraAlgorithm() {
	}
	public void execute(StationModel startModel) {
		for(StationModel model : StationModel.getStations()) {
			model.setTimeToHereFromStart(Integer.MAX_VALUE);
		}
		settled = new ArrayList<>();
		unsettled = new ArrayList<>();
		distance = new HashMap<>();
		distance.put(startModel, 0);
		startModel.setTimeToHereFromStart(0);
		unsettled.add(startModel);
		while(unsettled.size() > 0) {
			StationModel model = getMinimum(unsettled);
			settled.add(model);
			unsettled.remove(model);
			findMinimalDistances(model);
		}
		for(StationModel model : StationModel.getStations()) {
			if (model.getTimeToHereFromStart() == Integer.MAX_VALUE) {
				int minimalAdjacentTime = Integer.MAX_VALUE;
				for(StationModel adjModel : model.getAdjacentStationModels()) {
					if(adjModel.getTimeToHereFromStart() < minimalAdjacentTime) {
						minimalAdjacentTime = adjModel.getTimeToHereFromStart();
					}
					model.setTimeToHereFromStart(minimalAdjacentTime + model.getConnectionModelToDestination(adjModel).getTimeBetweenEndpoints());
				}
			}
		}
	}
	private void findMinimalDistances(StationModel model) {
		for(StationModel target : model.getAdjacentStationModels()) {
			if(getShortestDistance(target) > getShortestDistance(model) + getDistance(model,target)) {
				int time = getShortestDistance(model) + getDistance(model, target);
				distance.put(target, time);
				target.setTimeToHereFromStart(time);
                unsettled.add(target);
			}
		}
	}
	private int getDistance(StationModel model, StationModel target) {
		Collection<ConnectionModel> k = model.getOutgoingConnections();
		Collection<ConnectionModel> l = target.getOutgoingConnections();
		
		k.retainAll(l);
		
		int result = Integer.MAX_VALUE;
		if(k.size() != 1) {
			System.out.println("Error Computing Distance!");
			return result;
		}
		
		for(ConnectionModel cm : k) {
			 result = cm.getTimeBetweenEndpoints();
		}
		
		return result;
	}
	private StationModel getMinimum(ArrayList<StationModel> list) {
		StationModel minimum = null;
		for(StationModel model : list) {
			if(minimum == null) {
				minimum = model;
			}
			if(getShortestDistance(model) < getShortestDistance(minimum)) {
				minimum = model;
			}
		}
		return minimum;
	}
	private int getShortestDistance(StationModel minimum) {
		Integer d = distance.get(minimum);
		if (d == null) {
			return Integer.MAX_VALUE;
		} else {
			return d;
		}
	}

}
