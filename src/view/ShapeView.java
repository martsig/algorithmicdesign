package view;

public abstract class ShapeView {

	public abstract void draw();
}
