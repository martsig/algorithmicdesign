package view;

import java.util.ArrayList;

import main.Main;
import model.ShapeModel;
import model.StationModel;

public class LabelView extends ShapeView {

	private StationModel stationModel;
	private static ArrayList<LabelView> labelViews;
	
	public LabelView(StationModel stationModel) {
		this.stationModel = stationModel;
		if(labelViews == null) {
			labelViews = new ArrayList<>();
		}
	}
	@Override
	public void draw() {
		stationModel.getParent().textFont(Main.getFont(), Main.FONT_SIZE);
		stationModel.getParent().fill(0);
		stationModel.getParent().text(getDisplayName(stationModel.getName()), stationModel.getxPosition()-ShapeModel.DEFAULT_WIDTH, (float) (stationModel.getyPosition()+(ShapeModel.DEFAULT_HEIGHT+Main.FONT_SIZE/2.0)));
		//stationModel.getParent().text(stationModel.getTimeToHereFromStart()/60, stationModel.getxPosition()-ShapeModel.DEFAULT_WIDTH, (float) (stationModel.getyPosition()+(ShapeModel.DEFAULT_HEIGHT+Main.FONT_SIZE/2.0+1.2*Main.FONT_SIZE)));
	}
	
	public static ArrayList<LabelView> getLabelViews() {
		return labelViews;
	}

	public static void addToLabelViews(LabelView labelView) {
		if(!labelViews.contains(labelView)) {
			labelViews.add(labelView);
		}
	}
	
	private String getDisplayName(String name) {
		if(name.length() <= 7) {
			return name;
		}
		String leftPart = name.substring(0, 3);
		String rightPart = name.substring(3);
		rightPart = rightPart.replaceAll("[aeiou��� -]", "");
		if(rightPart.length() <= 4) {
			return leftPart+rightPart;
		}
		rightPart = rightPart.substring(0, 4);
		return leftPart+rightPart;
 
	}
}
