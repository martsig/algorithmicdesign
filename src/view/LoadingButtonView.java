package view;

import java.util.ArrayList;

import model.LoadingButtonModel;
import processing.core.PApplet;

public class LoadingButtonView extends ShapeView {

	private LoadingButtonModel model;
	private static ArrayList<LoadingButtonView> views;

	public LoadingButtonView(LoadingButtonModel model) {
		this.model = model;
	}
	
	@Override
	public void draw() {
		PApplet pane = model.getParent();
		pane.stroke(0);
		pane.fill(255,127,80);
		pane.ellipse(model.getxPosition(), model.getyPosition(), model.getWidth(), model.getHeight());
		
	}

	public static ArrayList<LoadingButtonView> getViews() {
		if(views == null) {
			views = new ArrayList<>();
		}
		return views;
	}

}
