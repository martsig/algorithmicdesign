package view;

import java.util.ArrayList;

import model.ConnectionModel;
import model.StationModel;

public class ConnectionView extends ShapeView {
	
	private ConnectionModel connectionModel;
	private static ArrayList<ConnectionView> connectionViewList;

	public ConnectionView(ConnectionModel connectionModel) {
		this.connectionModel = connectionModel;
	}
	@Override
	public void draw() {
		ArrayList<StationModel> endPoints = connectionModel.getEndPoints();
		if(endPoints.size() != 2) {
			return;
		}
		int x1 = endPoints.get(0).getxPosition();
		int y1 = endPoints.get(0).getyPosition();
		int x2 = endPoints.get(1).getxPosition();
		int y2 = endPoints.get(1).getyPosition();
		connectionModel.getParent().line(x1, y1, x2, y2);
	}
	public static ArrayList<ConnectionView> getConnectionViewList() {
		if(connectionViewList == null) {
			connectionViewList = new ArrayList<>();
		}
		return connectionViewList;
	}

}
