package view;

import java.util.ArrayList;

import model.StationModel;
import processing.core.PApplet;

public class StationView extends ShapeView {

	private StationModel stationModel;
	private static ArrayList<StationView> stationViews;

	public StationView(StationModel stationModel) {
		this.stationModel = stationModel;
			}

	@Override
	public void draw() {
		PApplet pane = stationModel.getParent();
		fillWithColor(pane);
		pane.ellipse(stationModel.getxPosition(), stationModel.getyPosition(), stationModel.getWidth(), stationModel.getHeight());
	}

	public static ArrayList<StationView> getStationViews() {
		return stationViews;
	}

	public void addToStationViews() {
		if(stationViews == null) {
			stationViews = new ArrayList<>();
		}
		stationViews.add(this);
	}

	private void fillWithColor(PApplet pane) {
		ArrayList<Integer> timesa = StationModel.getListOfTimes();
		int min = Integer.MAX_VALUE;
		int max = 0;
		for(int i : timesa) {
			if(i < min) {
				min = i;
			}
			if(i > max) {
				max = i;
			}
		}
		

		ArrayList<Integer> timesb = new ArrayList<>();
		for(int i : timesa) {
			if(i != max && i != min) {
				timesb.add(i);
			}
		}
		min = Integer.MAX_VALUE;
		max = 0;
		for(int i : timesb) {
			if(i < min) {
				min = i;
			}
			if(i > max) {
				max = i;
			}
		}
		int middle = (max + min)/2;
		int timeFromStart = stationModel.getTimeToHereFromStart();
		if(timeFromStart <= middle) {
			int rgb = 255 - (int) (timeFromStart*(255/((double) middle)));
			pane.fill(0, rgb, 0);
		}
		if(timeFromStart > middle){
			int rgb = (int) ((timeFromStart-middle)*(255/((double) middle)));
			pane.fill(rgb, 0, 0);
		}
	}

	public StationModel getStationModel() {
		return stationModel;
	}
}
