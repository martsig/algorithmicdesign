package lib;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;

public class StationImporter {

	private static HashMap<String, ArrayList<String>> stationList = new HashMap<>();
	private static HashMap<String, int[]> stationGridList = new HashMap<>();

	public static HashMap<String, ArrayList<String>> getStationList() {
		return stationList;
	}
	
	public static void loadStations() {
		BufferedReader buffererReader;
		try {
			buffererReader = new BufferedReader(
			        new InputStreamReader(new FileInputStream(System.getProperty("user.dir")+"\\src\\lib\\stations.txt")));
		    String line;
		    while ((line = buffererReader.readLine()) != null) {
		    	processStationInputLine(line);
		    }
		    buffererReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void loadStationPositions() {
		BufferedReader bufferedReader;
		try {
			bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(System.getProperty("user.dir")+"\\src\\lib\\stationgrid.txt")));
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				processGridInputLine(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void processGridInputLine(String line) {
		String[] parsedLine = parseLine(line);
		if(parsedLine != null && !stationGridList.containsKey(parsedLine[0])) {
			int[] pos = new int[2];
			pos[0] = Integer.parseInt(parsedLine[1]);
			pos[1] = Integer.parseInt(parsedLine[2]);
			stationGridList.put(parsedLine[0], pos);
		}
	}

	public static void writeOutAllStations() {
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(System.getProperty("user.dir")+"\\src\\lib\\stationsbare.txt")));
			for(String name : stationList.keySet()) {
				bw.write(name+"\n");
			}
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private static void processStationInputLine(String line) {
		String[] parsedLine = parseLine(line);
		ArrayList<String> adjacentStations = new ArrayList<>();
		if(parsedLine != null && !stationList.containsKey(parsedLine[0])) {
			for(int i = 1; i<parsedLine.length; i++) {
				if(parsedLine[i] != null && parsedLine[i] != "") {
					adjacentStations.add(parsedLine[i]);
				}
			}
			stationList.put(parsedLine[0], adjacentStations);
		}		
	}

	private static String[] parseLine(String line) {
		if(line.substring(0,1).equals("#")) {
			return null;
		}
		String[] parsedLine;
		parsedLine = line.split(":|;");
		return parsedLine;
	}

	public static HashMap<String, int[]> getStationGridList() {
		return stationGridList;
	}

}
