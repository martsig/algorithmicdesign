package model;

import java.util.ArrayList;

import processing.core.PApplet;

public class ConnectionModel extends ShapeModel {
	
	private ArrayList<StationModel> endPoints;
	private static ArrayList<ConnectionModel> connectionModelList;
	private int timeBetweenEndpoints;
	
	public ConnectionModel(PApplet pApplet, StationModel endpoint1, StationModel endpoint2) {
		super.setParent(pApplet);
		endPoints = new ArrayList<>();
		endPoints.add(endpoint1);
		endPoints.add(endpoint2);
	}
	
	public ArrayList<StationModel> getEndPoints() {
		return endPoints;
	}
	
	public static void addConnectionModelToList(ConnectionModel connectionModel) {
		if(connectionModelList == null) {
			connectionModelList = new ArrayList<>();
		}
		connectionModelList.add(connectionModel);
	}
	
	public boolean matchesExistingConnection() {
		if(connectionModelList == null) {
			connectionModelList = new ArrayList<>();
		}
		if(connectionModelList.isEmpty()) {
			return false;
		}
		for(ConnectionModel connectionModel : getConnectionModelList()) {
			if(endPoints.size() == 2 && connectionModel.getEndPoints().contains(endPoints.get(0)) && connectionModel.getEndPoints().contains(endPoints.get(1))) {
				return true;
			}
		}
		return false;
	}

	public static ArrayList<ConnectionModel> getConnectionModelList() {
		return connectionModelList;
	}

	public int getTimeBetweenEndpoints() {
		return timeBetweenEndpoints;
	}

	public void setTimeBetweenEndpoints(int timeBetweenEndpoints) {
		this.timeBetweenEndpoints = timeBetweenEndpoints;
	}
}
