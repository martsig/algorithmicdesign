package model;

import java.util.ArrayList;
import java.util.HashMap;

import lib.StationImporter;
import processing.core.PApplet;

public class StationModel extends ShapeModel {
	
	private String name;
	private static ArrayList<StationModel> stations;
	private ArrayList<StationModel> adjacentStations;
	private int timeToHereFromStart;
	private static ArrayList<Integer> listOfTimes;

	public StationModel(PApplet pApplet) {
		super.setParent(pApplet);
	}
	
	public StationModel(PApplet pApplet, String name){
		super.setParent(pApplet);
		this.name = name;
	}
	
	/**
	 * Call only when all stations are initialized
	 */
	public void setAdjacentStations() {
		if(adjacentStations == null) {
			adjacentStations = new ArrayList<>();
		}
		HashMap<String, ArrayList<String>> stationHashMap = StationImporter.getStationList();
		for(String stationName : stationHashMap.keySet()) {
			if(stationName.equals(this.name)) {
				for(String adjacentName : stationHashMap.get(stationName)) {
					for(StationModel stationModel : stations) {
						if(stationModel.getName().equals(adjacentName)) {
							adjacentStations.add(stationModel);
						}
					}
				}
			}
		}
	}
	
	public String getName() {
		return name;
	}
	
	public static void addStation(StationModel stationModel) {
		if(stations == null) {
			stations = new ArrayList<>();
		}
		for(StationModel model : stations) {
			if(model.getName().equals(stationModel.getName())) {
				return;
			}
		}
		stations.add(stationModel);
	}

	public ArrayList<StationModel> getAdjacentStationModels() {
		return adjacentStations;
	}

	public static ArrayList<StationModel> getStations() {
		return stations;
	}
	
	public ArrayList<ConnectionModel> getOutgoingConnections() {
		ArrayList<ConnectionModel> outgoingConnections = new ArrayList<>();
		for(ConnectionModel connectionModel : ConnectionModel.getConnectionModelList()) {
			if (connectionModel.getEndPoints().contains(this)) {
				outgoingConnections.add(connectionModel);
			}
		}
		return outgoingConnections;
	}

	public int getTimeToHereFromStart() {
		return timeToHereFromStart;
	}

	public void setTimeToHereFromStart(int timeToHereFromStart) {
		if (!getListOfTimes().contains(timeToHereFromStart)) {
			getListOfTimes().add(timeToHereFromStart);
		}
		this.timeToHereFromStart = timeToHereFromStart;
	}
	
	public ConnectionModel getConnectionModelToDestination(StationModel destination) {
		for(ConnectionModel cmodel : getOutgoingConnections()) {
			if(cmodel.getEndPoints().contains(destination)) {
				return cmodel;
			}
		}
		return null;
	}

	public static ArrayList<Integer> getListOfTimes() {
		if (listOfTimes == null) {
			listOfTimes = new ArrayList<>();
		}
		return listOfTimes;
	}

}
