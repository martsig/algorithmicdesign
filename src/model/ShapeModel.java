package model;

import processing.core.PApplet;

public abstract class ShapeModel {
	
	public static final int DEFAULT_HEIGHT = 20;
	public static final int DEFAULT_WIDTH = 20;

	private PApplet parent;
	private int xPosition;
	private int yPosition;
	private int height;
	private int width;
	
	public void setParent(PApplet pApplet) {
		this.parent = pApplet;
	}
	
	public PApplet getParent() {
		return parent;
	}

	public int getxPosition() {
		return xPosition;
	}

	public void setxPosition(int xPosition) {
		this.xPosition = xPosition;
	}

	public int getyPosition() {
		return yPosition;
	}

	public void setyPosition(int yPosition) {
		this.yPosition = yPosition;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}
}
