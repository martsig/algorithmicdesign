package model;

import processing.core.PApplet;

public class InputTextFieldModel {

	private PApplet parent;

	public InputTextFieldModel(PApplet pApplet) {
		this.parent = pApplet;
	}

	public PApplet getParent() {
		return parent;
	}

	public void setParent(PApplet pApplet) {
		this.parent = pApplet;
	}
}
