package model;

import processing.core.PApplet;

public class LoadingButtonModel extends MenuButtonModel {

	public LoadingButtonModel(PApplet parent) {
		super.setParent(parent);
		setxPosition(20);
		setyPosition(20);
		setWidth(20);
		setHeight(20);
	}
	
	
}
