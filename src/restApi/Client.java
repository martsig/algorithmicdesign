package restApi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.Normalizer;

import org.json.JSONException;
import org.json.JSONObject;

import model.StationModel;

public class Client {

	private final static String apiKey = "AIzaSyDoZy1xOcYftWVQdle7nisbabSZXRx-Zqs";
	// https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=Washington,DC&destinations=New+York+City,NY&key=YOUR_API_KEY

	/**
	 * 
	 * @return may return null
	 */
	private static JSONObject getRequest(String request) {
		JSONObject json = null;
		try {

			URL url = new URL(request);
			//URL url = new URL("https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=Washington,DC&destinations=New+York+City,NY&mode=transit&key="+apiKey);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode() + "\nCausing URL: " + request);
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			StringBuilder builder = new StringBuilder();
			 String line;
			    while ((line = br.readLine()) != null) {
			        builder.append(line);
			    }
			json = new JSONObject(builder.toString());
			conn.disconnect();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	private String buildGetRequest(StationModel model1, StationModel model2) {
		String request = "";
		request += "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins="
				+convertStringSpacesToPlus(model1.getName())
				+"&destinations="
				+convertStringSpacesToPlus(model2.getName())
				+"&language=de-DE"
				+"&mode=transit"
				+"&key="+apiKey;
		return request;
	}
	
	private String convertStringSpacesToPlus(String string) {
		string = Normalizer.normalize(string, Normalizer.Form.NFKC);
		string = string.replace("�", "ss");
		string = string.replace("�", "ae");
		string = string.replace("�", "oe");
		string = string.replace("�", "ue");
		return string.replaceAll("[ ]", "+")+"+Muenchen";
	}
	
	private int getTravelDuration(JSONObject json) {
		int value = Integer.MAX_VALUE;
		try {
			String[] splittedJsonAray = json.getJSONArray("rows").toString().split("value|distance");
			String valueAsString = "";
			if(!(splittedJsonAray.length>=2)) {
				return value;
			}
			for (Character c : splittedJsonAray[1].toCharArray()) {
				if(Character.isDigit(c)) {
					valueAsString += c;
				}
			}
			value = Integer.valueOf(valueAsString);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return value;
	}
	
	public int getTravelDurationInSeconds(StationModel model1, StationModel model2) {
		String request = buildGetRequest(model1, model2);
		JSONObject json = getRequest(request);
		int time = getTravelDuration(json);
		//System.out.println("Time between "+model1.getName()+" and "+model2.getName());
		return time;
	}
}
