package main;

import dijkstra.DijkstraAlgorithm;
import lib.StationImporter;
import model.ConnectionModel;
import model.LoadingButtonModel;
import model.ShapeModel;
import model.StationModel;
import processing.core.PApplet;
import processing.core.PFont;
import restApi.Client;
import view.ConnectionView;
import view.LabelView;
import view.LoadingButtonView;
import view.StationView;

public class Main extends PApplet {

	private static PFont font;
	private static final int SIZE_GRID_X = 16;
	private static final int SIZE_GRID_Y = 12;
	public static final int FONT_SIZE = 18;
	private String message = "reload";

	public static void main(String[] args) {
		PApplet.main("main.Main");
	}

	public void settings() {
		fullScreen();
	}

	public void setup() {
		font = createFont("Courier New", FONT_SIZE, false);
		StationImporter.loadStations();
		StationImporter.loadStationPositions();
		setStationModels();
		StationImporter.writeOutAllStations();
		setAdjacentStations();
		initializeConnections();
		initializeMenu();
	}
	
	private void initializeMenu() {
		LoadingButtonModel buttonModel= new LoadingButtonModel(this);
		LoadingButtonView view = new LoadingButtonView(buttonModel);
		LoadingButtonView.getViews().add(view);
	}

	@Override
	public void mouseClicked() {
		StationView view = getStationView(mouseX, mouseY);
		if(view != null) {
			DijkstraAlgorithm alg = new DijkstraAlgorithm();
			alg.execute(view.getStationModel());
		}
		if(mouseX < 40 && mouseX > 20 && mouseY < 40 && mouseY > 20) {
			updateRountine();
		}
		super.mouseClicked();
	}

	private void updateRountine() {
		Client client = new Client();
		int current = 1;
		int numberOfConnections = ConnectionModel.getConnectionModelList().size();
		for(ConnectionModel connectionModel : ConnectionModel.getConnectionModelList()) {
			connectionModel.setTimeBetweenEndpoints(client.getTravelDurationInSeconds(connectionModel.getEndPoints().get(0), connectionModel.getEndPoints().get(1)));
			fill(0);
			message = "Loading "+current+" of "+numberOfConnections;
			text(message, 50, 25);
			System.out.println(message);
			current++;
		}
		message = "loaded";
	}

	private StationView getStationView(int mouseX, int mouseY) {
		for(StationView view : StationView.getStationViews()) {
			if(Math.abs(view.getStationModel().getxPosition() - mouseX) <= StationModel.DEFAULT_WIDTH) {
				if(Math.abs(view.getStationModel().getyPosition() - mouseY) <= StationModel.DEFAULT_HEIGHT) {
					return view;
				}
			}
		}
		return null;
	}

	private void initializeConnections() {
		for(StationModel model : StationModel.getStations()) {
			for(StationModel adjacentModel : model.getAdjacentStationModels()) {
				ConnectionModel connectionModel = new ConnectionModel(this, model, adjacentModel);
				if(!connectionModel.matchesExistingConnection()) {
					ConnectionModel.addConnectionModelToList(connectionModel);
				}
			}
		}
		for(ConnectionModel connectionModel : ConnectionModel.getConnectionModelList()) {
			ConnectionView connectionView = new ConnectionView(connectionModel);
			ConnectionView.getConnectionViewList().add(connectionView);
			connectionModel.setTimeBetweenEndpoints((int) (Math.random()*1000));
		}
	}

	private void setAdjacentStations() {
		for(StationModel model : StationModel.getStations()) {
			model.setAdjacentStations();
		}
	}

	private void setStationModels() {
		for (String stationName : StationImporter.getStationList().keySet()) {
			StationModel model = new StationModel(this, stationName);
			StationModel.addStation(model);
			model.setHeight(ShapeModel.DEFAULT_HEIGHT);
			model.setWidth(ShapeModel.DEFAULT_WIDTH);
			if (StationImporter.getStationGridList().containsKey(model.getName())) {
				int[] pos = StationImporter.getStationGridList().get(model.getName());
				model.setxPosition(pos[0] * (width / SIZE_GRID_X));
				model.setyPosition(pos[1] * (height / SIZE_GRID_Y));
			} else {
				model.setxPosition(12);
				model.setyPosition(12);
			}
			StationView stationView = new StationView(model);
			stationView.addToStationViews();
			LabelView labelView = new LabelView(model);
			LabelView.addToLabelViews(labelView);
		}

	}

	public void draw() {
		background(255, 204, 0);
		textFont(Main.getFont(), Main.FONT_SIZE);
		textSize(18);
		fill(0);
		text(message, 50, 25);
		for (StationView stationView : StationView.getStationViews()) {
			stationView.draw();
		}
		for (LabelView labelView : LabelView.getLabelViews()) {
			labelView.draw();
		}
		for(ConnectionView connectionView : ConnectionView.getConnectionViewList()) {
			connectionView.draw();
		}
		for(LoadingButtonView view : LoadingButtonView.getViews()) {
			view.draw();
		}
	}

	public static PFont getFont() {
		return font;
	}

}
